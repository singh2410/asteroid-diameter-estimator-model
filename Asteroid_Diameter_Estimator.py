#!/usr/bin/env python
# coding: utf-8

# # Asteroid Diameter Estimators
# #By- Aarush Kumar
# #Dated: June 21,2021

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from random import seed 
seed(42)


# In[2]:


#Importing only the first 30000 rows
df = pd.read_csv('/home/aarush100616/Downloads/Projects/Asteroid Diameter Estimator/Asteroid.csv',nrows = 30000)


# In[3]:


df


# In[4]:


df.head()


# ## Exploratory Data Analysis

# In[5]:


df.info()


# In[6]:


df.isnull().sum()


# In[7]:


df.tail()


# In[8]:


df.describe()


# In[9]:


#Checking which columns(features) have nan values
for column in df.columns:
    print(column, df[column].isna().sum()/df.shape[0]) #returns the fraction of NAN values


# In[10]:


#Printing the first ten unique values of each feature
for column in df.columns:
    print(column, df[column].unique()[:10])


# In[12]:


#Steps 0
df['diameter']=pd.to_numeric(df['diameter'],errors='coerce') #transforming to numeric, setting errors to NaN
dropindexes = df['diameter'][df['diameter'].isnull()].index #rows with nan diameters to drop
dropped_df = df.loc[dropindexes] #saving dropped rows for the future
df = df.drop(dropindexes, axis=0) 


# In[13]:


#Steps 1
tooMuchNa = df.columns[df.isna().sum()/df.shape[0] > 0.5]
df = df.drop(tooMuchNa,axis=1)
df = df.drop(['condition_code','full_name'],axis=1)
df = df.drop(['neo','pha'],axis=1)


# In[14]:


#Step 2
df = df.fillna(df.mean())


# In[15]:


df.head()


# In[16]:


#Last sanity check for nan values
df.isna().values.any()


# In[17]:


df = df.drop(['albedo','H'],axis = 1)


# In[18]:


df['diameter']= df['diameter'].apply(np.log)
for column in df.columns.drop(['diameter']):
    df['log('+column+')']=df[column].apply(np.log)
df = df.dropna(axis=1)


# ## Correlation Analysis

# In[19]:


df.corr()['diameter'].abs().sort_values(ascending=False)


# ## Splitting the dataframe into train and test 

# In[20]:


from sklearn.model_selection import train_test_split
predictors = df.drop('diameter',axis=1) 
target = df['diameter']
X_train,X_test,Y_train,Y_test = train_test_split(predictors,target,test_size=0.20,random_state=0)


# In[21]:


X_train


# In[22]:


X_test


# In[23]:


Y_train


# In[24]:


Y_test


# ### Normalization

# In[25]:


from sklearn import preprocessing
#Input standard normalization:
std_scaler = preprocessing.StandardScaler().fit(X_train)
def scaler(X):
    x_norm_arr= std_scaler.fit_transform(X)
    return pd.DataFrame(x_norm_arr, columns=X.columns, index = X.index)
X_train_norm = scaler(X_train)
X_test_norm = scaler(X_test)
def inverse_scaler(X):
    x_norm_arr= std_scaler.inverse_transform(X)
    return pd.DataFrame(x_norm_arr, columns=X.columns, index = X.index)


# ## Trying different regressions and ranking them according to their R^2 score.

# In[26]:


from sklearn.metrics import r2_score
import seaborn as sns
def plot(prediction):
    fig, (ax1, ax2) = plt.subplots(1, 2,figsize=(20,7)) 
    sns.distplot(Y_test.values,label='test values', ax=ax1)
    sns.distplot(prediction ,label='prediction', ax=ax1)
    ax1.set_xlabel('Distribution plot')
    ax2.scatter(Y_test,prediction, c='orange',label='predictions')
    ax2.plot(Y_test,Y_test,c='blue',label='y=x')
    ax2.set_xlabel('test value')
    ax2.set_ylabel('estimated $\log(radius)$')
    ax1.legend()
    ax2.legend()
    ax2.axis('scaled') #same x y scale
def score(prediction):
    score = r2_score(prediction,Y_test)
    return score
def announce(score):
    print('The R^2 score achieved using this regression is:', round(score,3))
algorithms = []
scores = []


# ### Linear Regression

# In[27]:


#Defining the model
from sklearn.linear_model import LinearRegression
lr = LinearRegression()
###Training
lr.fit(X_train,Y_train)
###Predicting
Y_pred_lr = lr.predict(X_test)
###Scoring
score_lr = score(Y_pred_lr)
announce(score_lr)
algorithms.append('LR')
scores.append(score_lr)


# In[28]:


plot(Y_pred_lr)


# ### k-Nearest Neighbours regression

# In[29]:


from sklearn.neighbors import KNeighborsRegressor
neigh = KNeighborsRegressor(n_neighbors=3)
### Training
neigh.fit(X_train_norm,Y_train)
### Predicting 
Y_pred_neigh = neigh.predict(X_test_norm)
### Scoring
score_neigh=score(Y_pred_neigh)
announce(score_neigh)
algorithms.append('k-NN')
scores.append(score_neigh)


# In[30]:


plot(Y_pred_neigh)


# ### Decision Tree regression

# In[31]:


from sklearn import tree
decTree = tree.DecisionTreeRegressor()
### Training
decTree = decTree.fit(X_train_norm,Y_train)
### Predicting
Y_pred_tree = decTree.predict(X_test_norm)
### Scoring
score_tree = score(Y_pred_tree)
announce(score_tree)
algorithms.append('DTree')
scores.append(score_tree)


# In[32]:


plot(Y_pred_tree)


# ### Random Forest Regression

# In[33]:


from sklearn.ensemble import RandomForestRegressor
forest = RandomForestRegressor(max_depth=32, n_estimators=50)
### Training 
forest.fit(X_train_norm,np.ravel(Y_train))
###Predicting
Y_pred_forest = forest.predict(X_test_norm)
### Scoring
score_forest = score(Y_pred_forest)
announce(score_forest)
algorithms.append('RForest')
scores.append(score_forest)


# In[34]:


plot(Y_pred_forest)


# ### XGBoost regression

# In[37]:


import xgboost as xgb 
xgReg = xgb.XGBRegressor(objective ='reg:squarederror', colsample_bytree = 0.3, 
                         learning_rate = 0.08 ,
                max_depth = 4, n_estimators = 500)
### Training
xgReg.fit(X_train_norm,Y_train)
### Predicting
Y_pred_xgb = xgReg.predict(X_test_norm)
### Scoring
score_xgb = score(Y_pred_xgb)
announce(score_xgb)
algorithms.append('XGB')
scores.append(score_xgb)


# In[38]:


plot(Y_pred_xgb)


# ### One bonus of using xgboost is being able to simply see how important the different features where when creating the learners.

# In[40]:


fig, ax = plt.subplots(figsize=(10,10))
xgb.plot_importance(xgReg, height=0.5, ax=ax, importance_type='weight')
plt.show()


# ## Comparing all regression algorithms

# In[41]:


sns.barplot(algorithms,scores)

